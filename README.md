# HTML-Essentials-HomeTask

## HTML-Essentials-HomeTask

- [ ] [In addition to the main tags, the markup should contain the following tags: &lt;a&gt;, &lt;ul&gt;, &lt;li&gt;, &lt;h1&gt;, &lt;h2&gt;, &lt;h3&gt;, &lt;p&gt;, &lt;button&gt;, &lt;i&gt;, &lt;img&gt;, &lt;br&gt;
- [ ] [You have created a top menu using the &lt;ul&gt;, &lt;li&gt;, &lt;a&gt; tags
- [ ] [The markup which you have created should contain two different logo images: small and big
- [ ] [You have divided headings using the &lt;br&gt; tag
- [ ] [You have created the bottom button that contains ▶ symbol &#9654

## CSS-Basic-HomeTask

- [ ] [ou have applied 2 background images in the styles
- [ ] [You have commented the header section in HTML
- [ ] [You have not used the style attribute in HTML
- [ ] [You have written styles to one CSS file or several separate CSS files
- [ ] [Your markup matches the specific image in the task

